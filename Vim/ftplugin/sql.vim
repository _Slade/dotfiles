if exists("b:did_ftplugin")
    finish
end

syn keyword sqlKeyword after before cascade constraint constraints deferrable
syn keyword sqlKeyword deferred each foreign initially key materialized name
syn keyword sqlKeyword natural primary purge read references write

let b:didftplugin = 1
