if exists("b:did_ftplugin")
    finish
endif

setlocal autoindent
setlocal cindent
setlocal cinoptions+=j1,L0,:1s,=1s,b1,+
setlocal colorcolumn=80
setlocal complete=.,t,w
setlocal smartindent
setlocal suffixes=.bak,~,.o,.h,.info,.swp,.obj
setlocal synmaxcol=120

if HaveMakefile()
    setlocal makeprg=make
else
    setlocal makeprg=gcc\ -Wall\ -g\ -Og\ -std=iso9899:1999\ -o\ %:r:S\ %
endif

if g:loaded_tcomment
    call tcomment#DefineType('c', tcomment#GetLineC('// %s'))
endif

if exists(':Tabularize')
    " /* followed by as few anythings as possible ended by */
    AddTabularPattern! c  /\%(\/\*.\{-}\*\/\)\|\%(\/\/\)/
    AddTabularPattern! eq /[!=]\@<!==\@!/
endif

let b:did_ftplugin = 1
