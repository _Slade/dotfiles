if exists("b:did_ftplugin")
    finish
end

setlocal autoindent
setlocal cindent
setlocal cinoptions+=#0
setlocal colorcolumn=80
setlocal makeprg=perl6\ -c\ %
setlocal smartindent
setlocal synmaxcol=120
setlocal complete-=i " Don't scan included files

if exists(':Tabularize')
    AddTabularPattern! c      /\#.*$/ " Comments
    AddTabularPattern! arrow  /\%(^\|\s\)\@<==>\%($\|\s\)\@=/
    AddTabularPattern! eq     /[!=]\@<!==\@!/
end

let b:didftplugin = 1
