if exists("b:did_ftplugin")
    finish
end

if exists(':Tabularize')
    AddTabularPattern! c      /--.*$\|\%({-\|-}\)/
    AddTabularPattern! ie     /\<\%(if\|then\|else\)\>/
    AddTabularPattern! arrow  /\%(^\|\s\)\@<=->\%($\|\s\)\@=/
    AddTabularPattern! pipe   /\%(^\|[^|]\)\@<=|\%($\|[^|\)\@=/
    AddTabularPattern! eq     /[/=]\@<!==\@!/
end

if !filereadable(expand("%:p:h")."/Makefile")
    setlocal makeprg=ghc\ -Wall\ --make\ %
endif

setlocal colorcolumn=80

hi! link hsVarSym     Normal
hi! link hsStatement  Statement
hi! link hsString     Identifier
hi! link hsDelimiter  Normal

" hi! link VarId Normal
" hi! link ConId Normal
" hi! link hsString Identifier
" hi! link VarSymId Constant

let b:didftplugin = 1
