if exists("b:did_ftplugin")
    finish
end

setlocal colorcolumn=80
setlocal makeprg=python\ -m\ py_compile\ %
setlocal cinwords=if,elif,else,for,while,try,except,finally,def,class
setlocal synmaxcol=120

filetype indent plugin on
let g:pyindent_open_paren   = '&sw'
let g:pyindent_nested_paren = '&sw'
let g:pyindent_continue     = '0'

if exists(':Tabularize')
    AddTabularPattern! c  /\#.*$/ " Comments
    AddTabularPattern! :  /\%(^\|\s\)\@<==>\%($\|\s\)\@=/
    AddTabularPattern! eq /[!=]\@<!==\@!\|\<is\>/
end

let b:didftplugin = 1
