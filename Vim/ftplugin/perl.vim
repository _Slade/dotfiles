if exists("b:did_ftplugin")
    finish
end

setlocal noautoindent nocopyindent nocindent nosmartindent
filetype indent on
setlocal colorcolumn=80
setlocal keywordprg=perldoc\ -f
setlocal makeprg=perl\ -Mstrict\ -wc\ %
setlocal synmaxcol=120
setlocal complete-=i " Don't scan included files

let g:perl_sub_signatures = 1

if exists(':Tabularize')
    AddTabularPattern! c      /\#.*$/ " Comments
    AddTabularPattern! arrow  /\%(^\|\s\)\@<==>\%($\|\s\)\@=/
    AddTabularPattern! eq     /[!=]\@<!==\@!/
end

let b:didftplugin = 1
