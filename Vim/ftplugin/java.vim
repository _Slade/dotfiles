if exists("b:did_ftplugin")
    finish
endif

filetype indent on
setlocal colorcolumn=80
setlocal suffixes+=.class
setlocal suffixesadd=.java
setlocal synmaxcol=120

if HaveMakefile()
    setlocal makeprg=make
else
    setlocal makeprg=javac\ -Xlint\ %:S
endif

" Fold JavaDoc comments
syn region javadoc start="^\s*/\*\*" end="^.*\*/" transparent fold keepend

if exists(':Tabularize')
    AddTabularPattern! c  /\%(\/\*.\{-}\*\/\)\|\%(\/\/\)/
    AddTabularPattern! eq /[!=]\@<!==\@!/
endif

let g:ultisnips_java_brace_style = ''

let b:did_ftplugin = 1
