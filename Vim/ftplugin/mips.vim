if exists("b:did_ftplugin")
    finish
endif

setlocal autoindent
setlocal copyindent
setlocal incsearch
setlocal nocindent
setlocal noignorecase
setlocal nojoinspaces
setlocal nosmartindent
setlocal preserveindent

if !exists(':Tabularize')
    finish
endif

" Align comments
AddTabularPattern! ac /\#.\+$/

let b:didftplugin = 1
