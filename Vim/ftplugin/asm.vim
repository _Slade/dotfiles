setlocal autoindent
setlocal copyindent
setlocal incsearch
setlocal nocindent
setlocal noignorecase
setlocal nojoinspaces
setlocal nosmartindent
setlocal preserveindent

if exists(':Tabularize')
    AddTabularPattern! c /[#;].\+$/
endif
