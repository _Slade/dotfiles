if exists("b:did_ftplugin")
    finish
end

" Ignore temp and build files.
setlocal suffixes+=.aux,.bbl,.bcf,.blg,.log,.out,.run.xml,.pdf

" Default indenting is terrible, not needed for TeX anyway.
filetype indent off

" Check for makefile, otherwise use raw xelatex.
if !filereadable(expand("%:p:h")."/Makefile")
    setlocal makeprg=xelatex\ %
else
    setlocal makeprg=make
endif

if exists(':Tabularize')
    AddTabularPattern! c /%.*$/ " Comments
end

let b:didftplugin = 1
