if exists("b:did_ftplugin")
    finish
endif

setlocal makeprg=markdown\ %:S\ -o\ %:r:S.html
command! -nargs=0 -bang ViewMD AsyncRun<bang> markdown %:S | w3m -T text/html -dump

" Prevent insertion of * when wrapping a bullet point
setlocal formatoptions-=c,r,o
setlocal commentstring="<!-- %s -->"
setlocal comments=
setlocal sw=4 sts=4 ts=4 et

let b:did_ftplugin = 1
