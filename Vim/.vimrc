set nocompatible
let g:home = expand('~/')
let $VIM = g:home . '.vim'

" Appearance
syntax on
set t_Co=16
color solarized
set number
set synmaxcol=81
set shortmess=IOTfilnostwx
set noruler
set showcmd
highlight Search ctermfg=6
highlight Folded ctermfg=10 ctermbg=0 guifg=#4e4e4e guibg=#1c1c1c

filetype off

" Plugins
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'Indent-Guides'
Plugin 'SirVer/ultisnips'
Plugin 'godlygeek/tabular'
Plugin 'majutsushi/tagbar'
Plugin 'matchit.zip'
Plugin 'scrooloose/nerdtree'
Plugin 'skywind3000/asyncrun.vim'
Plugin 'tommcdo/vim-exchange'
Plugin 'tomtom/tcomment_vim'
Plugin 'tomtom/tlib_vim'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-unimpaired'

call vundle#end()

filetype on
filetype plugin on

let g:UltiSnipsSnippetDirectories  = [ $HOME . '/.vim/UltiSnips' ]
let g:UltiSnipsUsePythonVersion    = 2
let g:UltiSnipsJumpForwardTrigger  = '<c-k>'
let g:UltiSnipsJumpBackwardTrigger = '<c-l>'
let g:UltiSnipsEditSplit           = 'vertical'
inoremap <c-k> <c-k>

" Quickfix toggle using AsyncRun
nnoremap <Leader>q :call asyncrun#quickfix_toggle(8)<CR>

function! AbbrHome(dir)
    let l:home_pat = '^' . substitute($HOME, '\/', '\\/', '')
    return substitute(a:dir, l:home_pat, '~', '')
endfunc

" Statusline appearance
if has('statusline')
    set laststatus=2
    let linetype = { 'unix' : 'LF', 'mac' : 'CR', 'dos' : 'CRLF' }
    hi User1 ctermfg=7
    hi User2 ctermfg=6 ctermbg=0
    hi User3 ctermfg=12
    set statusline=
    set statusline+=[%1*%n%*]                              " Buffer number
    set statusline+=%y                                     " Filetype
    set statusline+=[%{empty(&fileencoding)?&encoding:&fileencoding}] " Encoding
    set statusline+=[%{linetype[&ff]}]                     " Line type
    set statusline+=%3*\ %<%{AbbrHome(expand('%:p:h'))}/%* " Filename 1
    set statusline+=%1*%{expand('%:t:r')}%*                " Filename 2
    set statusline+=%3*%{empty(expand('%:e'))?'':'.'.expand('%:e')}\ %* " Filename 3
    set statusline+=%{fugitive#statusline()}
    set statusline+=%m                                     " Modified flag
    set statusline+=%=                                     " Left/right separator
    " set statusline+=%.40{AbbrHome(getcwd())}\              " CWD
    set statusline+=[col\ %2*%2v%*]                        " Column
    set statusline+=[line\ %2*%2l%*\ of\ %2*%L%*]          " Line
    set statusline+=[%2*%2p%%%*]                           " Percentage
endif

" Indenting
set cinkeys-=0# " Don't dedent lines starting with '#'
set cinoptions=:0,l1,b1,t0,c4,(1s,m-1s,L0,#1
set expandtab copyindent preserveindent softtabstop=4 shiftwidth=4 tabstop=4
set formatoptions+=t,c,q,r
set linebreak " Break at characters in 'breakat' (i.e. not mid-word)
set noautoindent nosmartindent nocindent " Filetype plugins should enable these
set nojoinspaces
if exists('+breakindent')
    set breakindent
endif
filetype indent on
let g:indent_guides_auto_colors = 0
highlight IndentGuidesOdd  ctermbg=0
highlight IndentGuidesEven ctermbg=0

" Wrapping
set nowrap
set whichwrap=b,h,l,<,>,[,]
set textwidth=79
if exists('+colorcolumn')
    set colorcolumn=80
endif
if executable("par")
    set formatprg=par\ -w79
endif

" Searching
set hlsearch
set ignorecase
set smartcase
set incsearch
nnoremap n nzz
nnoremap N Nzz
nnoremap <Leader>/ :nohlsearch<CR>

" Ripgrep/rg
if executable("rg")
    set grepprg=rg\ --vimgrep\ --no-heading
    set grepformat=%f:%l:%c:%m,%f:%l:%m
endif

" Miscellaneous behavior
set backspace=indent,eol,start
set backupdir=/tmp/
set dir=/tmp/
set encoding=utf-8
set spelllang=en_us
set ttyfast
set wildignore=*/tmp/*,*.so,*.swp,*.zip,*.gz,*.jar,*.o,*.d,*.*~,*.a,*.class

" Make
command! -nargs=* -bang Make AsyncRun<bang> make <args>

" Save as root
command! W w !sudo tee % > /dev/null

command! Vbn vertical sbnext
command! -complete=buffer Vsb vertical sbuffer

" Tagbar plugin
let g:tagbar_sort = 0
let g:tagbar_left = 1
nnoremap <Leader>t :TagbarToggle<CR>

" NERDTree
nnoremap <Leader>f :NERDTreeToggle<CR>
let NERDTreeIgnore = ['\.class$', '\.o$']

" Perl options
let perl_include_pod   = 1
let perl_extended_vars = 1
let perl_sync_dist     = 250

" Window opening behavior
set splitright
set splitbelow
set switchbuf=useopen

" Make Y behave like D
nnoremap Y y$

" Toggle modifiability ('co<option>' similar to Unimpaired.vim mapping)
nnoremap com :setlocal modifiable!<CR>
" The 'p' is for 'preview'
nnoremap cop :setlocal readonly!<CR>

" Allow scrolling in insert mode
inoremap <C-E> <C-X><C-E>
inoremap <C-Y> <C-X><C-Y>

" Replace hjkl with jkl;
noremap j h
noremap ; l
noremap k gj
noremap l gk
noremap h ;
noremap zj zh
noremap z; zl
noremap zk zj
noremap zl zk
noremap zh z;
noremap zL zK
noremap zH z:

" Remap window switch keys
nnoremap <C-W>j <C-W>h
nnoremap <C-W>; <C-W>l
nnoremap <C-W>k <C-W>j
nnoremap <C-W>l <C-W>k
nnoremap <C-W>J <C-W>H
nnoremap <C-W>: <C-W>L
nnoremap <C-W>K <C-W>J
nnoremap <C-W>L <C-W>K

" Splits a line at cursor position
nnoremap <Leader><CR> i<CR><Esc>

" Line bubbling
nmap L [e
nmap K ]e
vmap L [egv
vmap K ]egv

" Cursor to bottom line (L mapped to bubble lines down)
nnoremap _ L

" Change the way commands/searches behave to allow Vim editing of commands
nnoremap : q:i
nnoremap / q/i
nnoremap ? q?i
nnoremap q: :
nnoremap q/ /
nnoremap q? ?
set cmdwinheight=3 " <C-c>x2 closes command window

if has('clipboard')
    " Mapping to copy/paste using clipboard
    vnoremap <Leader>y :yank +<CR>
    nnoremap <Leader>p :put +<CR>
    " Mapping to copy entire document to clipboard
    noremap <Leader>a :%yank +<CR>
    " Yank line without trailing newline
    nnoremap <Leader>yl 0"+y$
endif

" Jump to tag
nnoremap <Leader>j :tjump<Space>

" Useful macro to insert blocks.
inoremap {<CR> {<CR>}<Esc>:call BC_AddChar("}")<CR><Esc>kA<CR>
" inoremap ( ()<Esc>:call BC_AddChar(")")<CR>i
" inoremap [ []<Esc>:call BC_AddChar("]")<CR>i

function! BC_AddChar(schar)
   if exists('b:robstack')
       let b:robstack = b:robstack . a:schar
   else
       let b:robstack = a:schar
   endif
endfunction

" Remove trailing spaces from all lines of a buffer.
function! ClearTrailingSpaces()
    mark '
    %substitute/\s\+$//e
    nohlsearch
    normal `'
endfunction

" Check if there's a makefile in the current directory.
function! HaveMakefile()
    let l:names = ['Makefile', 'makefile']
    for f in l:names
        if filereadable(getcwd() . "/" . f)
            return v:true
        endif
    endfor
    return v:false
endfunction

" Convert a one-line parameter list to a multi-line one.
function! Listify() range
    if a:firstline != a:lastline
        execute a:firstline . ',' . a:lastline . "join"
    endif
    let save_pos    = getcurpos()
    let base_indent = repeat(' ', indent(a:firstline))
    let indent      = repeat(' ', &tabstop)
    let l = getline(a:firstline)
    " Split after first (
    let l = substitute(
\       l,
\       '\%([^(]\+\)\@<=(\s*',
\       '(\n' . base_indent . indent,
\       ''
\   )
    " Split on commas between arguments
    let l = substitute(l,
\       ",\\s*\\%(\\%([^\"']*[\"'][^\"']*[\"']\\)*[^\"']*$\\)\\@=",
\       ',\n' . base_indent . indent,
\       'g'
\   )
    " Split on last )
    let l = substitute(
\       l,
\       ')\%(.*)\)\@!',
\       '\n' . base_indent . ')',
\       ''
\   )
    call setline(a:firstline, "")
    call append(a:firstline - 1, split(l, '\n'))
    call setpos('.', save_pos)
endfunction

" Get the current date in YYYY-MM-DD format
function! Date()
    return strftime('%F', localtime())
endfunction

" Yank the visual selection into a register, joining the lines.
function! YankJoined(register) range
    let [lnum1, col1] = getpos("'<")[1:2]
    let [lnum2, col2] = getpos("'>")[1:2]
    let lines = getline(lnum1, lnum2)
    let lines[-1] = lines[-1][: col2 - (&selection == 'inclusive' ? 1 : 2)]
    let lines[0] = lines[0][col1 - 1:]
    call setreg(a:register, join(lines))
    if len(lines) > 1
        echom len(lines) . ' lines yanked'
    endif
endfunction

nnoremap <silent> <Leader>cws :call ClearTrailingSpaces()<CR>
" autocmd! BufWritePre * call ClearTrailingSpaces()
nnoremap <Leader>w :w<CR>
nnoremap <silent> <Leader>l :.call Listify()<CR>
vnoremap <silent> <Leader>l :call Listify()<CR>
vnoremap <silent> gy :call YankJoined("+")<CR>

" Digraphs
if has('digraphs')
    digraph GH 540 " Uppercase yogh
    digraph gh 541 " Lowercase yogh
    digraph Ss 383 " Long s
endif
