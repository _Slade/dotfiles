if [ -f /etc/bash_completion ]; then
	    . /etc/bash_completion
fi

xhost +local:root > /dev/null 2>&1

complete -cf sudo

shopt -s autocd
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
shopt -s globstar
shopt -s hostcomplete
shopt -s no_empty_cmd_completion
shopt -s nocaseglob

shopt -u histappend

export HISTCONTROL=ignoreboth
export HISTFILESIZE=${HISTSIZE}
export HISTSIZE=10000
export LC_CTYPE=en_US.UTF-8
export TERM=rxvt-unicode-256color
[[ $SHLVL == 2 ]] && export PATH="$PATH:/home/slade/Workspace/Scripts/"

alias ..='cd ../'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias bc='bc -lq'
alias cp='cp -i'     # confirm before overwriting something
alias df='df -h'     # human-readable sizes
alias du='du -h'
alias free='free -m' # show sizes in MB
alias grep='grep --color=tty -d skip'
alias less='less -R'
alias more='less -R'
alias mv='mv -i'
alias rm='rm -i'
alias sxiv='sxiv -oa'
alias vi='vim'

# Git
alias ga='git add'
alias gb='git branch'
alias gc='git commit'
alias gck='git checkout'
alias gd='git diff'
alias gls='git list-files'
alias gs='git status'

# local::lib
[[ $SHLVL == 2 ]] && eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"

function sprunge
{
    curl -F 'sprunge=<-' http://sprunge.us
}

function cap
{
    scrot -s /tmp/screenshot.png && \
    curl -F'file=@/tmp/screenshot.png' https://0x0.st
}

function erase
{
    echo -ne "\033c"
}

function perlop
{
    perl -MO=Concise,-exec "$@"
}


