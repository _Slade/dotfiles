Xhost +local:root > /dev/null 2>&1

# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct
zstyle ':completion:*' completions 1
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' glob 1
zstyle ':completion:*' list-dirs-first true
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' max-errors 1
zstyle ':completion:*' menu select=4
zstyle ':completion:*' original true
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' prompt 'Errors: %e'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' substitute 1
zstyle :compinstall filename '/home/slade/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# History
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
export HISTIGNORE='ls:ll:ld:ls -R:cd:cd -:pwd:exit'

# Colors
autoload -U colors && colors

# Settings
setopt AUTOCD
setopt AUTOCONTINUE
setopt BARE_GLOB_QUAL
setopt EXTENDEDGLOB
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
setopt INC_APPEND_HISTORY
setopt INTERACTIVE_COMMENTS
setopt NOTIFY
setopt NO_BEEP
setopt NO_CDABLE_VARS
setopt NO_CLOBBER
setopt NO_COMPLETE_ALIASES
setopt NO_MATCH
setopt NO_MULTIOS
setopt TRANSIENT_RPROMPT

setopt noflowcontrol
stty -ixon -ixoff

# local::lib
[[ $SHLVL == 2 ]] && eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"

# Help command
autoload -U run-help
autoload run-help-git
autoload run-help-svn
autoload run-help-svk
# I don't know a smarter way to do this yet (2015-08-04)
[[ $(whence run-help) != 'run-help' ]] && unalias run-help
alias help=run-help

# $PATH
typeset -U path # Make path discard duplicate entries
# path=(/home/slade/Workspace/Scripts/ $path)

source ~/.zkbd/rxvt-unicode-256color-:0.0
# Keybinds
bindkey -v
bindkey "^R" history-incremental-search-backward
bindkey "^U" kill-line
bindkey -M vicmd  "j" vi-backward-char
bindkey -M vicmd  "k" down-line-or-history
bindkey -M vicmd  "l" up-line-or-history
bindkey -M vicmd  ";" vi-forward-char
bindkey -M visual "j" vi-backward-char
bindkey -M visual "k" down-line-or-history
bindkey -M visual "l" up-line-or-history
bindkey -M visual ";" vi-forward-char
bindkey "${key[Home]}"     vi-beginning-of-line
bindkey "${key[End]}"      vi-end-of-line
bindkey "${key[Delete]}"   delete-char
bindkey "${key[PageUp]}"   vi-backward-blank-word
bindkey "${key[PageDown]}" vi-forward-blank-word

# Prompt string
PS1='%F{6}┌─[%F{3}%t%F{6}]─%(1j.[%F{4}%j%F{6}].)─[%F{1}%~%F{6}]
└>%(!.#.$)%f '
PS2='─> '
PS3='─> '
PS4='─+ '

# Aliases
alias ..='cd ../'
alias ...='cd ../../'
alias ..2='cd ../../'
alias ....='cd ../../../'
alias ..3='cd ../../../'
alias ..4='cd ../../../../'
alias ..5='cd ../../../../../'
alias ..6='cd ../../../../../../'
alias ..7='cd ../../../../../../../'
alias ..8='cd ../../../../../../../../'
alias ..9='cd ../../../../../../../../../'
alias ..10='cd ../../../../../../../../../../'
alias ..11='cd ../../../../../../../../../../../'
alias ..12='cd ../../../../../../../../../../../../'
alias ..13='cd ../../../../../../../../../../../../../'
alias ..14='cd ../../../../../../../../../../../../../../'
alias ..15='cd ../../../../../../../../../../../../../../../'
alias bc='bc -lq'
alias cdl='cd "$OLDPWD"'
alias cls='clear; ls'
alias convert='convert -verbose'
alias cp='cp -i'     # confirm before overwriting something
alias df='df -h'     # human-readable sizes
alias du='du -h'
alias erase="print -n '\ec'"
alias free='free -m' # show sizes in MB
alias grep='grep --color=tty -d skip'
alias la='ls -a'
alias ll='ls -l'
alias logs='journalctl -fn 20'
alias mogrify='mogrify -verbose'
alias more='less -Ri'
alias mv='mv -i'
alias perlop='perl -MO=Concise,-exec'
alias rm='rm -i'
alias sprunge='curl -F "sprunge=<-" http://sprunge.us'
alias sqlite=sqlite3
alias sxiv='sxiv -oa'
alias vi=vim
alias ytdl=youtube-dl

alias ls='ls --group-directories-first \
--time-style=+"%d.%m.%Y %H:%M" --color=auto -F'

# Git
alias g=git
# Stop 'g s' typos from running ghostscript
alias gs=':'

source ~/.zshfuncs

# vim: ft=zsh
