#!/usr/bin/sh
OPTS=CLghoptvx
rsync -$OPTS      \
    ~/.XCompose   \
    ~/.Xresources \
    ~/.astylerc   \
    ~/.bashrc     \
    ~/.gitconfig  \
    ~/.inputrc    \
    ~/.zshenv     \
    ~/.zshfuncs   \
    ~/.zshrc      \
    .
rsync -$OPTS ~/.vimrc Vim/.vimrc
rsync -$OPTS ~/.vim/UltiSnips/*.snippets Vim/UltiSnips/
rsync -$OPTS ~/.vim/ftplugin/*.vim Vim/ftplugin/
