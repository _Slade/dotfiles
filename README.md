These are most of my dotfiles. I use separate branches and git cherry-pick to
manage versions across different machines.

You probably don't want to be on the master branch; it's not maintained.
